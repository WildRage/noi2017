#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
int getrand(){
	return ((1ll*rand()<<16)+rand())%998244353; 
}
int n=1000000000,K=1000;
int change(int K){
	if (K<=50) return K;
	if (K<=200) return K-rand()%5;
	if (K<=1000) return K-rand()%60;
	return K-rand();
}
int main(){
	srand(time(0)); 
	freopen("pool20.in","w",stdout);
	int x=getrand(),y=getrand();
	while (x==y||x==0||y==0) x=getrand(),y=getrand();
	if (x>y) swap(x,y);
	cout<<change(n)<<" "<<change(K)<<" "<<x<<" "<<y<<endl;
	return 0;
}
