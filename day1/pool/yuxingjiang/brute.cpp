#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
#define MOD 998244353
using namespace std;

struct int_Z {
	int v;
	int_Z(int v = 0) { this->v = v; }
};
int_Z operator * (const int_Z &a, const int_Z &b) { return (int_Z) {(long long) a.v * b.v % MOD}; };
int_Z operator + (const int_Z &a, const int_Z &b) { int x = a.v + b.v; if (x >= MOD) x -= MOD; return (int_Z) {x}; };

int_Z p[1010][1010], f[(int) 1e6+10];

int ex(int x, int y, int w) {
	if ((w = (w + y) % y) % x == 0)
		return w / x;
	return ((long long) ex(y % x, x, -w) * y + w) / x;
}
int prob(int n, int k, int x, int y) {
	int_Z Q, P;
	P.v = ((long long) x * ex(y, MOD, 1) % MOD);
	Q.v = (1 - P.v + MOD) % MOD;
	
	cerr << P.v << " " << Q.v << endl;

	memset(p, 0, sizeof(p));

	for (int i = k+1; i >= 0; --i)
		p[i][0].v = 1;
		
	for (int len = 1; len <= k; ++len) {
		for (int dep = k/len; dep >= 0; --dep) {
			int_Z &u = p[dep][len];
			int_Z fac_P(1);
			
			u = 0;
			for (int l1 = 0; l1 < len; ++l1) {
				u = u + p[dep+1][l1]*p[dep][len-l1-1]*Q*fac_P;
				fac_P = fac_P*P;
			}
			u = u + p[dep+1][len]*fac_P; 
		}
	}
	
	f[0].v = 1;
	for (int i = 1; i <= n; ++i) {
		f[i].v = 0;
		
		int_Z fac_P(1);
		for (int j = 0; j <= k && j <= i; ++j) {
			if (i-j == 0)
				f[i] = f[i]+p[1][j]*fac_P;
			else
				f[i] = f[i]+f[i-j-1]*p[1][j]*Q*fac_P;
			fac_P = fac_P * P;
		}
	}
	return f[n].v;
}
int main() {
	int n, k, x, y;
	long long ans;
	
	cin >> n >> k >> x >> y;
	
	ans = (prob(n, k, x, y) - prob(n, k-1, x, y) + MOD) % MOD;
	cout << ans << endl;
}
