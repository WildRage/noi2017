#include <cstdio>
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
	FILE *out = fopen(argv[1], "r");
	FILE *ans = fopen(argv[2], "r");
	int ct = 0;
	while (!feof(out) && !feof(ans))
	{
		++ct;
		int x, y;
		fscanf(out, "%d", &x);
		fscanf(ans, "%d", &y);
		if (x != y)
		{
			cerr << "Error: the " << ct << "-th query" << endl;
			return 1;
		}
	}
	if (!feof(out) || !feof(ans))
	{
		cerr << "Error: different length" << endl;
		return 1;
	}

	return 0;
}