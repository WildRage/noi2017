import sys
import os

x = 0
first = True
cnt = 0
for line in open("../data/11.in"):
	if first:
		first = False
		continue
	l = line.split(" ")
	cnt += 1
	if len(l) == 3:
		x += int(l[1]) << int(l[2])
		y = x
		ct = 0
		while y > 0:
			tmp = y & ((1L << 60) - 1L)
			if tmp != 0:
				print ct, tmp
			y >>= 60
			ct += 1
		print cnt
		if (cnt > 100):
			break
		#raw_input()
	else:
		print (x >> int(l[1])) & 1
