#include <cstdio>
#include <assert.h>
#include <iostream>
using namespace std;

void get(long long &x)
{
	int tmp;
	scanf("%d", &tmp);
	x = (long long) tmp;
}

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	long long x = 0;
	for (int t = 0; t < n; ++t)
	{
		int sign;
		scanf("%d", &sign);
		if (sign == 1)
		{
			long long a, b;
			get(a); get(b);
			x += a*(1LL<<b);
			if (x < 0)
			{
				cerr << "Error: x < 0\t";
				return 1;
			}
		}
		else
		{
			long long k;
			get(k);
			if (x & (1 << k))
				printf("1\n");
			else
				printf("0\n");
		}
	}

	return 0;
}