// integer naive_new.cpp[nm/1024] by wys @2017-06-27

#include <stdio.h>
#include <stdint.h>

const int MAXN = 1000005;
const int MAXM = MAXN * 30 + 100;
const int MAXLEN = MAXM / 32 + 1;
const int MAXLEN2 = MAXLEN / 32 + 1;

uint32_t A[MAXLEN];
uint32_t is_0[MAXLEN2];
uint32_t is_1[MAXLEN2];

void get(int pos) {
	if (is_0[pos >> 5] & (1u << (pos & 31))) {
		A[pos] = 0;
	}
	if (is_1[pos >> 5] & (1u << (pos & 31))) {
		A[pos] = -1;
	}
}

void update(int pos) {
	uint32_t tmp = A[pos];
	if (tmp == 0) {
		is_0[pos >> 5] |= 1u << (pos & 31);
	} else {
		is_0[pos >> 5] &= ~(1u << (pos & 31));
	}
	if (tmp == -1) {
		is_1[pos >> 5] |= 1u << (pos & 31);
	} else {
		is_1[pos >> 5] &= ~(1u << (pos & 31));
	}
}

void _add1(int p) {
	for (; p & 31; p++) {
		get(p), ++A[p], update(p);
		if (A[p] != 0) return;
	}
	
	int q = p >> 5;
	while (is_1[q] == -1) {
		is_0[q] = -1;
		is_1[q] = 0;
		++q;
	}
	
	p = q << 5;
	
	for (; ; p++) {
		get(p), ++A[p], update(p);
		if (A[p] != 0) return;
	}
}

void _sub1(int p) {
	for (; p & 31; p++) {
		get(p), --A[p], update(p);
		if (A[p] != -1) return;
	}
	
	int q = p >> 5;
	while (is_0[q] == -1) {
		is_1[q] = -1;
		is_0[q] = 0;
		++q;
	}
	
	p = q << 5;
	
	for (; ; p++) {
		get(p), --A[p], update(p);
		if (A[p] != -1) return;
	}
}

void _add(int pos, uint32_t val) {
	get(pos);
	if ((A[pos] += val) < val) {
		_add1(pos + 1);
	}
	update(pos);
}

void _sub(int pos, uint32_t val) {
	get(pos);
	if (A[pos] < val) {
		_sub1(pos + 1);
	}
	A[pos] -= val;
	update(pos);
}

void add(int _a, int b) {
	if (_a == 0) return;
	
	bool neg = _a < 0;
	uint32_t a = neg ? -_a : _a;
	
	// b/32, b%32, 32-b%32, b/32+1, 0, b%32
	
	int pos = b >> 5;
	int off = b & 31;
	int len1 = 32 - off;
	
	// log(1e9) = 30
	uint32_t part1 = len1 >= 30 ? a : (a & ((1u << len1) - 1));
	uint32_t part2 = len1 >= 30 ? 0 : (a >> len1);
	
	if (part1) (neg ? _sub : _add)(pos, part1 << off);
	if (part2) (neg ? _sub : _add)(pos + 1, part2);
}

int query(int k) {
	return get(k >> 5), ((A[k >> 5]) >> (k & 31)) & 1u;
}

int main() {
	
	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	
	for (int i = 0; i < MAXLEN2; i++) {
		is_0[i] = -1;
	}
	
	for (int i = 0; i < n; i++) {
		int op, a, b, k;
		scanf("%d", &op);
		if (op == 1) {
			scanf("%d%d", &a, &b);
			add(a, b);
		} else {
			scanf("%d", &k);
			putchar("01"[query(k)]);
			putchar('\n');
		}
	}
}
