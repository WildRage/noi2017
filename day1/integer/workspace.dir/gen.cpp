// integer gen by wys @2017-06-27

#include <stdio.h>
#include <stdint.h>

const int MAXN = 1000005;
const int MAXM = MAXN * 30;
const int MAXLEN = MAXM / 32 + 5;

namespace Integer {
	
	struct node {
		node *l, *r;
		uint32_t val;
		int tag;
		
		void down() {
			if (tag != 233) {
				l->val = l->tag = tag;
				r->val = r->tag = tag;
			}
		}
		
		void update() {
			if (l->tag == r->tag) {
				tag = l->tag;
			} else {
				tag = 233;
			}
		}
	};
	
	inline int get_tag(uint32_t x) {
		return x == 0 ? 0 : x == -1 ? -1 : 233;
	}
	
	node _nodes[MAXLEN * 2], *_next_node = _nodes;
	
	int len;
	node *root;
	
	node * build(int l, int r) {
		node *ret = _next_node++;
		ret->val = ret->tag = 0;
		
		if (l < r) {
			int mid = (l + r) >> 1;
			ret->l = build(l, mid);
			ret->r = build(mid + 1, r);
		}
		
		return ret;
	}
	
	bool add1(node *root, int l, int r) {
		if (l == r) {
			bool ret = (++root->val) == 0;
			root->tag = get_tag(root->val);
			return ret;
		}
		if (root->tag == -1) {
			root->tag = 0;
			return 1;
		}
		root->down();
		int mid = (l + r) >> 1;
		add1(root->l, l, mid) && add1(root->r, mid + 1, r);
		root->update();
		return 0;
	}
	
	bool sub1(node *root, int l, int r) {
		if (l == r) {
			bool ret = (--root->val) == -1;
			root->tag = get_tag(root->val);
			return ret;
		}
		if (root->tag == 0) {
			root->tag = -1;
			return 1;
		}
		root->down();
		int mid = (l + r) >> 1;
		sub1(root->l, l, mid) && sub1(root->r, mid + 1, r);
		root->update();
		return 0;
	}
	
	bool add(node *root, uint32_t a, int pos, int l, int r) {
		if (l == r) {
			bool ret = (root->val += a) < a;
			root->tag = get_tag(root->val);
			return ret;
		}
		root->down();
		int mid = (l + r) >> 1;
		bool ret;
		if (pos <= mid) {
			ret = add(root->l, a, pos, l, mid) && add1(root->r, mid + 1, r);
		} else {
			ret = add(root->r, a, pos, mid + 1, r);
		}
		root->update();
		return ret;
	}
	
	bool sub(node *root, uint32_t a, int pos, int l, int r) {
		if (l == r) {
			bool ret = root->val < a;
			root->val -= a;
			root->tag = get_tag(root->val);
			return ret;
		}
		root->down();
		int mid = (l + r) >> 1;
		bool ret;
		if (pos <= mid) {
			ret = sub(root->l, a, pos, l, mid) && sub1(root->r, mid + 1, r);
		} else {
			ret = sub(root->r, a, pos, mid + 1, r);
		}
		root->update();
		return ret;
	}
	
	void _add(node *root, uint32_t a, int pos) {
		add(root, a, pos, 0, len);
	}
	
	void _sub(node *root, uint32_t a, int pos) {
		sub(root, a, pos, 0, len);
	}
	
	void add(node *root, int _a, int b) {
		if (_a == 0) return;
		bool neg = _a < 0;
		uint32_t a = neg ? -_a : _a;
		
		int pos = b >> 5;
		int off = b & 31;
		int len = 32 - off;
		
		uint32_t part1 = len >= 30 ? a : (a & ((1u << len) - 1));
		uint32_t part2 = len >= 30 ? 0 : (a >> len);
		
		if (part1) (neg ? _sub : _add)(root, part1 << off, pos);
		if (part2) (neg ? _sub : _add)(root, part2, pos + 1);
	}
	
	uint32_t query(node *root, int pos, int l, int r) {
		if (l == r) {
			return root->val;
		}
		root->down();
		int mid = (l + r) >> 1;
		if (pos <= mid) {
			return query(root->l, pos, l, mid);
		} else {
			return query(root->r, pos, mid + 1, r);
		}
	}
	
	int query(node *root, int k) {
		return (query(root, k >> 5, 0, len) >> (k & 31)) & 1;
	}
	
	void init(int n, int t1, int t2, int t3) {
		int m;
		if (t2 == 1) {
			m = 30;
		} else if (t2 == 2) {
			m = 100;
		} else if (t2 == 3) {
			m = n;
		} else {
			m = 30 * n;
		}
		m += 100;
		
		m += 100;
		
		len = m / 32 + 1;
		
		_next_node = _nodes;
		root = build(0, len);
	}
	
	void add(int a, int b) {
		add(root, a, b);
	}
	
	int query(int k) {
		return query(root, k);
	}
}

int seed = 23333;

inline int get_rand() {
	return seed = seed * 16807LL % 2147483647;
}

inline long long get_rand_2() {
	return get_rand() * (1LL << 31) + get_rand();
}

inline int rand_int(int l, int r) {
	if (r - l == 1) {
		return l + ((get_rand() >> 5) & 1);
	}
	return get_rand_2() % (r - l + 1) + l;
}

void gen(int n, int t1, int t2, int t3) {
	Integer::init(n, t1, t2, t3);
	
	printf("%d %d %d %d\n", n, t1, t2, t3);
	
	seed = (seed * 16807LL + n * 1000 + t1 * 100 + t2 * 10 + t3) % 2147483647;
	
	int m = t2 == 1 ? 30 : t2 == 2 ? 100 : t2 == 3 ? n : 30 * n;
	int max_a = t1 == 3 ? 1000000000 : 1;
	
	int state = -1;
	int last_a = -1;
	int last_b = -1;
	
	for (int i = 0; i < n; i++) {
		int op, a, b, k;
		
		if (t1 == 1) {
			if (t3 == 1) {
				op = i < n * 0.65 ? 1 : 2;
			} else {
				op = rand_int(1, 2);
			}
			if (op == 1) {
				a = 1;
				b = rand_int(0, m);
			} else {
				k = rand_int(0, m);
			}
		} else {
			
			if (i == 0) {
				op = 1;
				a = rand_int(1, max_a);
				b = rand_int(m * 0.95, m);
				Integer::add(a, b);
				Integer::add(1, m + 50);
			} else if (i < n * 0.1) {
				if (t3 == 1) {
					op = 1;
				} else {
					op = rand_int(1, 2);
				}
				if (op == 1) {
					while (1) {
						if (rand_int(1, 2) == 1) {
							a = rand_int(1, max_a);
							b = rand_int(m * 0.95, m);
						} else {
							a = rand_int(1, max_a) * (rand_int(0, 1) * 2 - 1);
							b = rand_int(0, m * 0.1);
						}
						Integer::add(a, b);
						if (Integer::query(m + 50) == 1) {
							break;
						} else {
							Integer::add(-a, b);
						}
					}
				} else if (op == 2) {
					k = rand_int(0, m);
				}
			} else if (m > 100 && i < n * 0.75) {
				if (t3 == 2 && rand_int(1, 5) == 1) {
					op = 2;
					k = rand_int(0, m);
				} else {
					op = 1;
					
					if (state == -1) {
						if (Integer::query(m * 0.5) == 1) {
							state = 1;
						} else {
							state = 0;
						}
					}
					
					if (state == 1) {
						if (last_a == -1) {
							last_a = rand_int(0, max_a);
							last_b = rand_int(m * 0.12, m * 0.17);
							a = last_a;
							b = last_b;
							state = 0;
						} else {
							a = last_a;
							b = last_b;
							state = 0;
							last_a = last_b = -1;
						}
					} else {
						if (last_a == -1) {
							last_a = rand_int(0, max_a);
							last_b = rand_int(m * 0.12, m * 0.17);
							a = -last_a;
							b = last_b;
							state = 1;
						} else {
							a = -last_a;
							b = last_b;
							state = 1;
							last_a = last_b = -1;
						}
					}
					
					Integer::add(a, b);
				}
			} else {
				if (t3 == 1) {
					op = i < n * 0.75 ? 1 : 2;
				} else {
					op = rand_int(1, 2);
				}
				if (op == 1) {
					while (1) {
						a = rand_int(1, max_a) * (rand_int(0, 1) * 2 - 1);
						b = rand_int(0, m);
						Integer::add(a, b);
						if (Integer::query(m + 50) == 1) {
							break;
						} else {
							Integer::add(-a, b);
						}
					}
				} else {
					k = rand_int(0, m);
				}
			}
		}
		
		if (op == 1) {
			printf("1 %d %d\n", a, b);
		} else {
			printf("2 %d\n", k);
		}
	}
}

int main(int argc, char **argv) {
	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	
	if (argc == 1 + 1) {
		sscanf(argv[1], "%d", &seed);
	}
	
	gen(n, t1, t2, t3);
}
