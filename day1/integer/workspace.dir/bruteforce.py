n = int(raw_input().split(" ")[0])

x = 0

for i in xrange(n):
	l = raw_input().split(" ")
	if len(l) == 3:
		x += int(l[1]) << int(l[2])
	else:
		print (x >> int(l[1])) & 1
