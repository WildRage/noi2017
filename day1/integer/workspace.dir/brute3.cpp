// integer/brute[3][nm][int] by wys @2017-06-27

#include <stdio.h>

const int MAXN = 1000005;
const int MAXM = MAXN * 30 + 100;

int A[MAXM];

void add1(int p) {
	while (A[p]) A[p++] = 0;
	A[p] = 1;
}

void sub1(int p) {
	while (!A[p]) A[p++] = 1;
	A[p] = 0;
}

void add(int a, int b) {
	if (a < 0) {
		a = -a;
		for (int i = 0; i < 30; i++) {
			if ((a >> i) & 1) {
				sub1(b + i);
			}
		}
	} else {
		for (int i = 0; i < 30; i++) {
			if ((a >> i) & 1) {
				add1(b + i);
			}
		}
	}
}

int query(int k) {
	return A[k];
}

int main() {
	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	
	for (int i = 0; i < n; i++) {
		int op, a, b, k;
		scanf("%d", &op);
		if (op == 1) {
			scanf("%d%d", &a, &b);
			add(a, b);
		} else {
			scanf("%d", &k);
			printf("%d\n", query(k));
		}
	}
}
