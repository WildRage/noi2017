// integer/brute[2][nm/4] by wys @2017-06-27

#include <stdio.h>

const int MAXN = 1000005;
const int MAXM = MAXN * 30 + 100;

int _A[MAXM / 4 + 1];
bool *A = (bool*)_A;

void add1(int p) {
	for (; p & 3; p++) {
		if (A[p] == 0) return A[p] = 1, void();
		A[p] = 0;
	}
	
	int *a = _A + p / 4;
	for (; *a == 0x01010101; a++) {
		*a = 0;
	}
	
	p = (a - _A) << 2;
	
	while (A[p]) A[p++] = 0;
	A[p] = 1;
}

void sub1(int p) {
	for (; p & 3; p++) {
		if (A[p]) return A[p] = 0, void();
		A[p] = 1;
	}
	
	int *a = _A + p / 4;
	for (; *a == 0; a++) {
		*a = 0x01010101;
	}
	
	p = (a - _A) << 2;
	
	while (!A[p]) A[p++] = 1;
	A[p] = 0;
}

void add(int a, int b) {
	if (a < 0) {
		a = -a;
		for (int i = 0; i < 30; i++) {
			if ((a >> i) & 1) {
				sub1(b + i);
			}
		}
	} else {
		for (int i = 0; i < 30; i++) {
			if ((a >> i) & 1) {
				add1(b + i);
			}
		}
	}
}

int query(int k) {
	return A[k];
}

int main() {
	int n, t1, t2, t3;
	scanf("%d%d%d%d", &n, &t1, &t2, &t3);
	
	for (int i = 0; i < n; i++) {
		int op, a, b, k;
		scanf("%d", &op);
		if (op == 1) {
			scanf("%d%d", &a, &b);
			add(a, b);
		} else {
			scanf("%d", &k);
			printf("%d\n", query(k));
		}
	}
}
