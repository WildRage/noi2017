#include <stdio.h>

int n_cases = 25;

int main() {
	freopen("../tables/data_range.json", "w", stdout);
	puts("[");
	puts("	[\"测试点编号\", \"$n$ \\\\le\", \"$t_1$\", \"$t_2$\", \"$t_3$\"],");
	
	for (int i = 1; i <= n_cases; i++) {
		char name[100];
		sprintf(name, "../data/%d.in", i);
		FILE *f = fopen(name, "r");
		int n, t1, t2, t3;
		fscanf(f, "%d%d%d%d", &n, &t1, &t2, &t3);
		
		printf("	[\"%d\", \"%d\", \"%d\", \"%d\", \"%d\"]", i, n, t1, t2, t3);
		if (i < n_cases) putchar(',');
		putchar('\n');
		
		fclose(f);
	}
	
	puts("]");
}