#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cmath>

const int N = 2011, M = 2011, V = 311, inf = 0x3F3F3F3F;
int n, m, v, e, dist[V][V], basic[N], extend[N];
double pro[N], rec[N][M][2];

void input(){
	memset(dist, 0x3F, sizeof(dist));
	scanf("%d%d%d%d", &n, &m, &v, &e);
	for(int i = 1; i <= v; i++)
		dist[i][i] = 0;
	for(int i = 1; i <= n; i++)
		scanf("%d", &basic[i]);
	for(int i = 1; i <= n; i++)
		scanf("%d", &extend[i]);
	for(int i = 1; i <= n; i++)
		scanf("%lf", &pro[i]);
	for(int i = 1; i <= e; i++){
		int u, v, w;
		scanf("%d%d%d", &u, &v, &w);
		if(dist[u][v] > w)
			dist[u][v] = dist[v][u] = w;
	}
}

void floyd(){
	for(int k = 1; k <= v; k++)
		for(int i = 1; i <= v; i++)
			for(int j = 1; j <= v; j++)
				dist[i][j] = std::min(dist[i][j], dist[i][k] + dist[k][j]);
	/*for(int i = 1; i <= v; i++){
		for(int j = 1; j <= v; j++)
			printf("%d ", dist[i][j]);
		printf("\n");
	}*/
}

void dp(){
	for(int j = 0; j <= m; j++)
		rec[1][j][0] = rec[1][j][1] = inf;
	rec[1][0][0] = rec[1][1][1] = 0;
	for(int i = 2; i <= n; i++){
		rec[i][0][0] = rec[i - 1][0][0] + dist[basic[i - 1]][basic[i]];
		rec[i][0][1] = inf;
		for(int j = 1; j <= m; j++){
			rec[i][j][0] = std::min(
				rec[i - 1][j][0] + dist[basic[i - 1]][basic[i]],
				rec[i - 1][j][1] + dist[basic[i - 1]][basic[i]] * (1 - pro[i - 1]) + dist[extend[i - 1]][basic[i]] * pro[i - 1]
			);
			rec[i][j][1] = std::min(
				rec[i - 1][j - 1][0] + dist[basic[i - 1]][basic[i]] * (1 - pro[i]) + dist[basic[i - 1]][extend[i]] * pro[i],
				rec[i - 1][j - 1][1] + 
					dist[basic[i - 1]][basic[i]] * (1 - pro[i - 1]) * (1 - pro[i]) +
					dist[basic[i - 1]][extend[i]] * (1 - pro[i - 1]) * pro[i] +
					dist[extend[i - 1]][basic[i]] * pro[i - 1] * (1 - pro[i]) +
					dist[extend[i - 1]][extend[i]] * pro[i - 1] * pro[i]
			);
		}
	}
	/*for(int i = 1; i <= n; i++){
		for(int j = 0; j <= m; j++)
			printf("(%.3f,%.3f) ", rec[i][j][0], rec[i][j][1]);
		printf("\n");
	}*/
	double ans = inf;
	for(int i = 0; i <= m; i++){
		ans = std::min(ans, rec[n][i][0]);
		ans = std::min(ans, rec[n][i][1]);
	}
	printf("%.2f\n", ans);
	if(fabs(ans - int(ans * 100 + .5) * .01) > 4e-3)
		printf("Error diff %.6f\n", ans);
}

int main(){
	input();
	floyd();
	dp();
	return 0;
}
