#include "testlib.h"
#include <cstdio>

using namespace std;

void print_usage(const char *exe) {
    printf("%s: n m q_min q_max v_min v_max t a_min a_max\n", exe);
}

// 1 <= n <= 1e5, 0 <= m <= 1e7, 0 < u < v <= 1e9, 0 <= q <= 200, 1 <= t <= 101, 
// Format:
//1 0 0 1 2 1
//n m q u v t
//a1 a2 ... an
//

int main(int argc, const char *argv[]) {
    registerValidation();
    if (argc != 10) {
        print_usage(argv[0]);
        return -1;
    }
    int an = atoi(argv[1]);
    int am = atoi(argv[2]);
    int q_min = atoi(argv[3]);
    int q_max = atoi(argv[4]);
    int v_min = atoi(argv[5]);
    int v_max = atoi(argv[6]);
    int at = atoi(argv[7]);
    int a_min = atoi(argv[8]);
    int a_max = atoi(argv[9]);

    int n = inf.readInt(an, an, "n");
    inf.readSpace();
    int m = inf.readInt(am, am, "m");
    inf.readSpace();
    int q = inf.readInt(q_min, q_max, "q");
    inf.readSpace();
    int u = inf.readInt();
    inf.readSpace();
    int v = inf.readInt(v_min, v_max, "v");
    inf.readSpace();
    ensuref(0 < u && u < v, "0 < u < v");
    int t = inf.readInt(at, at, "t");
    inf.readEoln();
    // printf("xx\n");
    for (int i = 0; i < n; i++) {
        int tmp_a = inf.readInt(a_min, a_max, "a");
        if (i != n - 1)  inf.readSpace();
    }
    // printf("xx2\n");
    inf.readEoln();
    inf.readEof();

    return 0;
}