#include <bits/stdc++.h>

int R()
{
	int r = rand() & 32767;
	return (rand() & 32767) * 32768 + r;
}

int main(int argc, char **argv)
{
	FILE *con = fopen("con", "w");
	if(argc != 7)
	{
		fprintf(con, "Wrong Parameter\n");
		return 1;
	}
	// n m maxA maxY maxQ t
	int n, m, maxA, maxY, maxQ, t;
	sscanf(argv[1], "%d", &n);
	sscanf(argv[2], "%d", &m);
	sscanf(argv[3], "%d", &maxA);
	sscanf(argv[4], "%d", &maxY);
	sscanf(argv[5], "%d", &maxQ);
	sscanf(argv[6], "%d", &t);
	srand(n + m * 233 + maxA * 666 + maxY * 77777 + maxQ * 171792544 + t * 130233666);
	int x, y;
	do
	{
		x = R() % maxY + 1;
		y = R() % maxY + 1;
	} while(x >= y);
	printf("%d %d %d %d %d %d\n", n, m, R() % (maxQ + 1), x, y, t);
	for(int i = 1; i <= n; i++) printf("%d%c", R() % (maxA + 1), " \n"[i == n]);
	return 0;
}
