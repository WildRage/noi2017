#include <cstdio>
#include <queue>

std::priority_queue<int> Q;

int main(){
	int n, m, q, x, y, t;
	scanf("%d%d%d%d%d%d", &n, &m, &q, &x, &y, &t);
	while(n--){
		int c; scanf("%d", &c);
		Q.push(c);
	}
	int mark = 0, oc = 0; bool first = true;
	while(m--){
		int c = Q.top() + mark;
		Q.pop();
		int ca = (long long) c * x / y;
		int cb = c - ca;
		mark += q;
		Q.push(ca - mark); Q.push(cb - mark);
		if(++oc == t){
			oc = 0;
			if(first) first = false; else putchar(' ');
			printf("%d", c);
		}
	}
	putchar('\n');
	oc = 0; first = true;
	while(!Q.empty()){
		int c = Q.top() + mark;
		Q.pop();
		if(++oc == t){
			oc = 0;
			if(first) first = false; else putchar(' ');
			printf("%d", c);
		}
	}
	putchar('\n');
	return 0;
}
