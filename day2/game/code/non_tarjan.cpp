#include <cstdio>
#include <vector>
#include <cstring>
#include <algorithm>
using namespace std;

const int maxm = 100000*2;
const int maxn = 50000*2;
const int maxc = 3;
const int maxd = 8;

struct Edge_t
{
	int x, y, cx, cy;
};

struct Edge_t edge[maxm];

struct E_t
{
	int r, next;
};

struct E_t e[maxm];

vector<int> xEdge[maxd];

char S[maxn];
int NofNode, nodeId[maxn][maxc];
int n, d, m, newId[maxn], tim, vis[maxn];
int line, head[maxn], ans[maxn], cnt;

void input()
{
	scanf("%d%d",&n, &d);
	scanf("%s", S);
	scanf("%d", &m);
	for (int i = 0; i < m; ++i)
	{
		int x, y;
		char cx, cy, s1, s2;
		scanf("%d%c%c%d%c%c", &x, &s1, &cx, &y, &s2, &cy);
		edge[i].x = x-1;
		edge[i].y = y-1;
		edge[i].cx = (int)(cx-'A');
		edge[i].cy = (int)(cy-'A');
	}
}

void addEdge(int x, int y)
{
	e[line].r = y;
	e[line].next = head[x];
	head[x] = line++;
}

void delEdge(int x, int y)
{
	head[x] = e[--line].next;
}

int another(int x, int j)
{
	for (int i = 0; i < maxc; ++i)
		if (i != j && nodeId[x][i] != -1)
			return nodeId[x][i];
}

void AddEdge(int x, int cx, int y, int cy)
{
	if (nodeId[x][cx] != -1)
	{
		if (nodeId[y][cy] == -1)
			addEdge(nodeId[x][cx], another(x, cx));
		else
		{
			addEdge(nodeId[x][cx], nodeId[y][cy]);
			if (x != y) addEdge(another(y, cy), another(x, cx));
		}
	}
}

void DelEdge(int x, int cx, int y, int cy)
{
	if (nodeId[x][cx] != -1)
	{
		if (nodeId[y][cy] == -1)
			delEdge(nodeId[x][cx], another(x, cx));
		else
		{
			if (x != y) delEdge(another(y, cy), another(x, cx));
			delEdge(nodeId[x][cx], nodeId[y][cy]);
		}
	}
}

void ready()
{
	int d_ct = 0;
	for (int i = 0; i < n; ++i)
		if (S[i] == 'x')
		{
			newId[i] = n-d+d_ct;
			++d_ct;
		}
		else
		{
			newId[i] = i-d_ct;
			S[i-d_ct] = S[i];
		}
	for (int i = n-d; i < n; ++i)
		S[i] = 'x';
	for (int i = 0; i < m; ++i)
	{
		edge[i].x = newId[edge[i].x];
		edge[i].y = newId[edge[i].y];
	}

	NofNode = 0;
	for (int i = 0; i < n-d; ++i)
		for (int j = 0; j < maxc; ++j)
			nodeId[i][j] = (S[i] == (char)('a'+j))? -1 : NofNode++;

	line = 0;
	memset(head, 0xff, sizeof(head));
	for (int i = 0; i < m; ++i)
		if (S[edge[i].x] == 'x' || S[edge[i].y] == 'x')
		{
			int tmp;
			if (S[edge[i].x] == 'x' && S[edge[i].y] == 'x')
				tmp = max(edge[i].x, edge[i].y);
			else
				tmp = (S[edge[i].x] == 'x')? edge[i].x : edge[i].y;
			xEdge[tmp-(n-d)].push_back(i);
		}
		else
			AddEdge(edge[i].x, edge[i].cx, edge[i].y, edge[i].cy);
}

void extendGraph(int x)
{
	int id = x-(n-d);
	for (int i = 0; i < xEdge[id].size(); ++i)
	{
		int j = xEdge[id][i];
		AddEdge(edge[j].x, edge[j].cx, edge[j].y, edge[j].cy);
	}
}

void resumeGraph(int x)
{
	int id = x-(n-d);
	for (int i = xEdge[id].size()-1; i >= 0; --i)
	{
		int j = xEdge[id][i];
		DelEdge(edge[j].x, edge[j].cx, edge[j].y, edge[j].cy);
	}
}

const int R = 1;
const int B = 2;

bool non_tarjan(int u)
{
	if (vis[u] >= tim && (vis[u]&3) == B) return false;
	if (vis[u] >= tim && (vis[u]&3) == R) return true;
    vis[u] = tim | R;
    vis[u^1] = tim | B;
    ans[cnt++] = u;
    for (int i = head[u]; i != -1; i = e[i].next)
		if (!non_tarjan(e[i].r)) return false;
	return true;
}

bool check()
{
	tim += 4;
	for (int i = 0; i < NofNode; ++i)
	{
		if (vis[i] > tim) continue;
		cnt = 0;
		if (!non_tarjan(i))
		{
			for (int j = 0; j < cnt; ++j)
			{
				vis[ans[j]] = tim;
				vis[ans[j]^1] = tim;
			}
			if (!non_tarjan(i^1)) return false;
		}
	}
	return true;
}

bool dfs(int i)
{
	if (!check()) return false;
	if (i >= n) return true;
	for (char ch = 'a'; ch <= 'c'; ++ch)
	{
		S[i] = ch;
		for (int j = 0; j < maxc; ++j)
			nodeId[i][j] = (S[i] == (char)('a'+j))? -1 : NofNode++;
		extendGraph(i);
		if (dfs(i+1)) return true;
		resumeGraph(i);
		NofNode -= 2;
	}
	return false;
}

void solve()
{
	tim = 0;
	memset(vis, 0, sizeof(vis));

	if (!dfs(n-d))
		printf("-1\n");
	else
	{
		for (int i = 0; i < n; ++i)
			for (int j = 0; j < maxc; ++j)
				if (nodeId[newId[i]][j] != -1 && (vis[nodeId[newId[i]][j]]&3) == R)
					printf("%c", (char)('A'+j));
		printf("\n");
	}
}

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	input();
	ready();
	solve();

	fclose(stdin);
	fclose(stdout);

	return 0;
}