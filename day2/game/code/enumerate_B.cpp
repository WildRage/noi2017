#include <cstdio>
#include <cstring>
#include <iostream>

using namespace std;

const int maxn = 50000;
const int maxm = 50000;

string S;
char cx[maxm], cy[maxm];
int n, d, m, x[maxm], y[maxm];

int main(int argc, char **argv)
{
	freopen(argv[1], "r", stdin);
	freopen(argv[2], "w", stdout);

	cin >> n >> d;
	cin >> S;
	cin >> m;
	for (int i = 0; i < m; ++i)
	{
		char tcx, tcy;
		cin >> x[i] >> cx[i] >> y[i] >> cy[i];
		--x[i]; --y[i];
	}

	bool flag;
	for (int s = 0; s < (1 << (n+d)); ++s)
	{
		flag = true;
		string tmpAns = "";
		for (int i = 0, j = 0; i < n && flag; ++i)
		{
			int tmp;
			if (S[i] == 'x')
			{
				tmp = (s & (3 << j)) >> j;
				if (tmp == 3) flag = false;
				j += 2;
			} else
			{
				tmp = (s & (1 << j)) >> j;
				if (S[i] == 'a' || S[i] == 'b' && tmp == 1) ++tmp;
				++j;
			}
			tmpAns.push_back((char)('A'+tmp));
		}
		if (!flag) continue;
		for (int i = 0; i < m && flag; ++i)
			if (tmpAns[x[i]] == cx[i] && tmpAns[y[i]] != cy[i]) flag = false;
		if (flag)
		{
			cout << tmpAns << endl;
			break;
		}
	}
	if (!flag)
		cout << -1 << endl;

	fclose(stdin);
	fclose(stdout);

	return 0;
}