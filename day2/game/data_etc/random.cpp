#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;

const int maxn = 50000;

pair<int,char> S[maxn]; 

int main(int argc, char **argv)
{
	bool only_c = argc > 4;

	srand((unsigned)time(NULL));

	freopen(argv[1], "w", stdout);

	int n = atoi(argv[2]);
	int d = atoi(argv[3]);
	cout << n << ' ' << d << endl;

	for (int i = 0; i < n-d; ++i)
	{
		S[i].first = rand();
		if (only_c)
			S[i].second = 'c';
		else
			S[i].second = (char)('a'+rand()%3);
	}
	for (int i = n-d; i < n; ++i)
	{
		S[i].first = rand();
		S[i].second = 'x';
	}
	sort(S, S+n);
	for (int i = 0; i < n; ++i)
		cout << S[i].second;
	cout << endl;

	int m = 2*n;
	cout << m << endl;
	for (int i = 0; i < m; ++i)
	{
		int x = rand()%n+1;
		int y = rand()%n+1;
		char cx = (char)(rand()%3 + 'A');
		char cy = (char)(rand()%3 + 'A');
		cout << x << ' ' << cx << ' ' << y << ' ' << cy << endl;
	}

	fclose(stdout);
	return 0;
}