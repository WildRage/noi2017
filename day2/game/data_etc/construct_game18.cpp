#include <cstdio>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <assert.h>

using namespace std;

const int maxn = 50000;

char A[maxn], S[maxn];
pair<int,char> T[maxn]; 

int main(int argc, char **argv)
{
	srand(1);
	freopen(argv[1], "w", stdout);

	int n = 50000;
	int d = 0;
	bool only_c = false;

	cout << n << ' ' << d << endl;
	for (int i = 0; i < n-d; ++i)
	{
		T[i].first = rand();
		if (only_c)
			T[i].second = 'c';
		else
			T[i].second = (char)('a'+rand()%3);
	}
	for (int i = n-d; i < n; ++i)
	{
		T[i].first = rand();
		T[i].second = 'x';
	}
	sort(T, T+n);
	for (int i = 0; i < n; ++i)
	{
		S[i] = T[i].second;
		cout << S[i];
	}
	cout << endl;

	for (int i = 0; i < n; ++i)
		if (i >= n/4)
			A[i] = (S[i] == 'c')? 'B' : 'C';
		else
			A[i] = (S[i] == 'a')? 'B' : 'A';

	int m = n;
	cout << m << endl;
	for (int i = 1; i < n; ++i)
	{
		if (i > n/4)
		{
			char cx = 'C', cy = 'C';
			while (A[i-1] == cx || S[i-1] == (char)(cx-'A'+'a')) --cx;
			while (A[i] == cy || S[i] == (char)(cy-'A'+'a')) --cy;
			cout << i << ' ' << cx << ' ' << i+1 << ' ' << cy << endl;
		}
		else
		{
			char cx = 'A', cy = 'A';
			while (A[i-1] == cx || S[i-1] == (char)(cx-'A'+'a')) ++cx;
			while (A[i] == cy || S[i] == (char)(cy-'A'+'a')) ++cy;
			cout << i << ' ' << cx << ' ' << i+1 << ' ' << cy << endl;	
		}
	}
	char cx = 'C';
	while (A[n-2] == cx || S[n-2] == (char)(cx-'A'+'a')) --cx;
	cout << n-1 << ' ' << cx << ' ' << n << ' ' << A[n-1] << endl;

	fclose(stdout);
	return 0;
}