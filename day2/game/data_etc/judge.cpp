#include <cstdio>
#include <cstring>
#include <sstream>
#include <iostream>
#include <cstdlib>
using namespace std;

const int maxn = 50000;

int n, d, m;
char S[maxn], A[maxn];

void error(string x)
{
	cerr << x << endl;
	exit(0);
}

string ItoS(int x)
{
	string y;
	stringstream f;
	f << x; f >> y;
	return y;
}

int main(int argc, char **argv)
{
	if (argc < 4)
	{
		FILE *in = fopen(argv[1], "r");
		FILE *ans = fopen(argv[2], "r");

		fscanf(ans, "%s", A);
		if (strcmp(A, "-1") == 0) error("-1");
		fscanf(in, "%d%d", &n, &d);
		if (strlen(A) != n) error("-2");
		fscanf(in, "%s", S);
		for (int i = 0; i < n; ++i)
			if (A[i] == (char)(S[i]-'a'+'A')) error("1");
		fscanf(in, "%d", &m);
		for (int i = 0; i < m; ++i)
		{
			int x, y;
			char cx, cy, s1, s2;
			fscanf(in, "%d%c%c%d%c%c", &x, &s1, &cx, &y, &s2, &cy);
			if (A[x-1] == cx && A[y-1] != cy) error("2 " + ItoS(i));
		}
		cerr << "AC" << endl;
	}
	else
	{
		FILE *in = fopen(argv[1], "r");
		FILE *std = fopen(argv[2], "r");
		FILE *ans = fopen(argv[3], "r");

		fscanf(std, "%s", S);
		fscanf(ans, "%s", A);
		if (strcmp(S, "-1") == 0)
		{
			if (strcmp(A, "-1") == 0)
			{
				cerr << "AC" << endl;
				return 0;
			}
			return 1;
		}
		if (strcmp(A, "-1") == 0) return 1;

		fscanf(in, "%d%d", &n, &d);
		if (strlen(A) != n) return 1;
		fscanf(in, "%s", S);
		for (int i = 0; i < n; ++i)
			if (A[i] == (char)(S[i]-'a'+'A')) return 1;
		fscanf(in, "%d", &m);
		for (int i = 0; i < m; ++i)
		{
			int x, y;
			char cx, cy, s1, s2;
			fscanf(in, "%d%c%c%d%c%c", &x, &s1, &cx, &y, &s2, &cy);
			if (A[x-1] == cx && A[y-1] != cy) return 1;
		}
		cerr << "AC" << endl;
	}

	return 0;
}