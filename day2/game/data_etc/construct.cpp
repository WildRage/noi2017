#include <cstdio>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;

const int maxn = 50000;

char A[maxn];
pair<int,char> S[maxn]; 

int main(int argc, char **argv)
{
	bool only_c = argc > 4;

	srand((unsigned)time(NULL));
	int n = atoi(argv[2]);
	int d = atoi(argv[3]);
	int m = n * 2;

	for (int i = 0; i < n; ++i)
	{
		S[i].first = rand();
		if (only_c)
			S[i].second = (char)('A'+rand()%2);
		else
			S[i].second = (char)('A'+rand()%3);
	}
	sort(S, S+n);
	for (int i = 0; i < n; ++i)
		A[i] = S[i].second;

	freopen(argv[1], "w", stdout);
	cout << n << ' ' << d << endl;
	for (int i = 0; i < n-d; ++i)
	{
		S[i].first = rand();
		if (only_c)
			S[i].second = 'c';
		else
			S[i].second = (char)('a'+rand()%3);
	}
	for (int i = n-d; i < n; ++i)
	{
		S[i].first = rand();
		S[i].second = 'x';
	}
	sort(S, S+n);

	for (int i = 0; i < n; ++i)
	{
		if (only_c)
		{
			if (S[i].second == 'x')
				cout << 'x';
			else
				cout << 'c';
		}
		else
		{
			if (S[i].second != ((char)(A[i]-'A'+'a')))
				cout << S[i].second;
			else
			{
				if (S[i].second == 'c')
					cout << 'a';
				else
					cout << (char)(S[i].second+1);
			}
		}
	}
	cout << endl;
	cout << m << endl;

	for (int i = 0; i < m; ++i)
	{
		int x, y;
		do
		{
			x = rand()%n+1;
			y = rand()%n+1;
		} while (x == y);
		cout << x << ' ' << A[x-1] << ' ' << y << ' ' << A[y-1] << endl;
	}

	fclose(stdout);
	return 0;
}