#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string>

using namespace std;

string ItoS(int x)
{
	string y;
	stringstream f;
	f << x; f >> y;
	return y;
}

int run(string command)
{
	return system(command.c_str());
}

int main(int argc, char **argv)
{
	string inCode = "/home/lch/noi2017/day2/game/data/construct";
	string outCode = "/home/lch/noi2017/day2/game/code/search_B";

	string inFile = "game13.in";
	string outFile = "game13.ans";
	int n = 100;
	int d = 8;
	int m = 100;

	int ct = 0;
	string ans;
	do
	{
		cerr << "ct = " << ++ct << endl;
		run(inCode + " " + inFile + " " + ItoS(n) + " " + ItoS(d) + " " + ItoS(m));
		run(outCode + " " + inFile + " " + outFile);
		ifstream fin(outFile.c_str());
		fin >> ans;
	} while (ans == "-1");

	return 0;
}