#include <cstdio>
#include <vector>
#include <algorithm>
#define f(x, y, z) for(int x = (y); x <= (z); ++x)
using namespace std;
typedef long long ll;
typedef pair<int, int> Item;

int n, m, k, mq;
typedef int Array[100007];
Array a, s, c, x, q, g;
ll ans[100007];

struct Queue{
	vector<Item> p;
	void init(int t){
		p.push_back(Item(0, 1e9));
		f(i, 1, n){
			int d;
			if(x[i]) d = min(mq, (c[i] + x[i] - 1) / x[i]);
			else d = mq;
			if(t < d) p.push_back(Item(a[i], x[i]));
			else if(t == d) p.push_back(Item(a[i], c[i] - (t - 1) * x[i] - 1)), p.push_back(Item(a[i] + s[i], 1));
		}
		sort(p.begin(), p.end());
	}
	int top(){
		while(!p.empty() && !p.back().second) p.pop_back();
		if(p.empty()) return 0;
		return p.back().first;
	}
	void pop(){
		--p.back().second;
	}
} Q[100007];

int main()
{
	scanf("%d%d%d", &n, &m, &k);
	f(i, 1, n) scanf("%d%d%d%d", a + i, s + i, c + i, x + i);
	f(i, 1, k) scanf("%d", q + i), mq = max(mq, q[i]);
	f(i, 1, mq) Q[i].init(i);
	f(t, 1, mq){
		ans[t] = ans[t - 1];
		f(_, 1, m){
			int l = 1, sg = 0;
			f(i, 1, mq) if((sg += g[i]) >= m * i) l = i + 1;
			int mx = -1, mi, c;
			f(i, l, mq) if((c = Q[i].top()) > mx) mx = c, mi = i;
			Q[mi].pop();
			ans[t] += mx;
			++g[mi];
		}
	}
	f(i, 1, k) printf("%lld\n", ans[q[i]]);
	return 0;
}
