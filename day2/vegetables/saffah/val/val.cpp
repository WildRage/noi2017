#include "testlib.h"
using std::to_string;

int MAXN[26] = {0, 
	2, 3, 4, 1000, 1000, 1000,
	4, 6, 8, 10, 20,
	100, 100, 100, 100,
	1000, 1000, 1000, 1000, 1000,
	100000, 100000, 100000, 100000, 100000
};

int MAXM[26] = {0,
	10, 10, 10, 10, 10, 10,
	1, 2, 1, 2, 3,
	10, 10, 10, 10,
	10, 10, 10, 10, 10,
	10, 10, 10, 10, 10
};

int MAXP[26] = {0,
	1000, 1000, 1000, 2, 3, 4,
	4, 6, 8, 10, 20,
	100, 100, 100, 100,
	1000, 1000, 1000, 1000, 1000,
	100000, 100000, 100000, 100000, 100000
};

int ALLS0[26] = {0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
	1, 0, 0, 0,
	1, 1, 0, 0, 0,
	1, 1, 0, 0, 0
};

int ALLX0[26] = {0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
	0, 1, 0, 0,
	1, 0, 1, 0, 0,
	1, 0, 1, 0, 0
};

int testcase;
#define maxn (MAXN[testcase])
#define maxm (MAXM[testcase])
#define maxp (MAXP[testcase])
#define alls0 (ALLS0[testcase])
#define allx0 (ALLX0[testcase])

const int e9 = 1000000000;

int main(int argc, char **argv) {
	
	if (argc != 2) {
		fprintf(stderr, "usage: %s <test_case_id>\n", argv[0]);
		return 1;
	}
	
	registerValidation();
	
	ensuref(sscanf(argv[1], "%d", &testcase) == 1, "case");
	ensuref(1 <= testcase && testcase <= 25, "case");
	
	int n = inf.readInt(1, maxn, "n");
	inf.readSpace();
	int m = inf.readInt(1, maxm, "m");
	inf.readSpace();
	int k = inf.readInt(1, e9, "k");
	inf.readEoln();
	
	for(int i = 1; i <= n; i++)
	{
		int a = inf.readInt(1, e9, "a_" + to_string(i));
		inf.readSpace();
		int s = inf.readInt(0, alls0 ? 0 : e9, "s_" + to_string(i));
		inf.readSpace();
		int c = inf.readInt(1, e9, "c_" + to_string(i));
		inf.readSpace();
		int x = inf.readInt(0, allx0 ? 0 : e9, "x_" + to_string(i));
		inf.readEoln();
	}
	
	static int vis[1000007];
	for(int i = 1; i <= k; i++)
	{
		int p = inf.readInt(0, maxp, "p_" + to_string(i));
		ensuref(!vis[p], "duplicate p_%d and p_%d", vis[p], i);
		vis[p] = i;
		inf.readEoln();
	}
	
	inf.readEof();
	
	printf("valid case %d\n", testcase);
	
	return 0;
}
