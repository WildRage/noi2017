#include<cstdlib>
#include<cstring>
#include<cstdio>
#include<set> 
#include<queue>
#define INF 2147483647
#define MAXV 100010
#define MAXE 1000100
#define MAXN 100010
#define MAXK 100010
using namespace std;
int n,m,k;
int a[MAXN+10],s[MAXN+10],f[MAXN+10],x[MAXN+10];
int q[MAXK+10];
long long ans[MAXK+10];
int MAXP;
long long ANS;
int k00[MAXN+10];
int zgg=0,la,gf0=0,tl=0;
int fa[MAXN+10];
int pla[MAXN+10],cnt[MAXN+10],sp[MAXN+10];
struct node
{
	int pla,key;
};
bool operator < (node c1,node c2)
{
	return c1.key<c2.key;
}
priority_queue<node> w[MAXK+10];
int minn[MAXK*10],lazy[MAXK*10],minp[MAXK*10];//退流
int maxx[MAXK*10],maxp[MAXK*10];
void build_tree(int now,int head,int tail)
{
	minn[now]=0;
	minp[now]=tail;
	if(head==tail)
	{
		if(w[head].size()>0)
			maxx[now]=w[head].top().key;
		else
			maxx[now]=-INF;
		maxp[now]=head;
		return;
	}
	int mid=head+tail>>1;
	build_tree(now<<1,head,mid);
	build_tree(now<<1|1,mid+1,tail);
	if(maxx[now<<1]>maxx[now<<1|1])
	{
		maxx[now]=maxx[now<<1];
		maxp[now]=maxp[now<<1];
	}
	else
	{
		maxx[now]=maxx[now<<1|1];
		maxp[now]=maxp[now<<1|1];
	}
}
void merge_rgs(int now,int ct)
{
	if(~ct)
	{
		minn[now]=0;
		minp[now]=ct;
		return;
	}
	if(minn[now<<1]+lazy[now<<1]<minn[now<<1|1]+lazy[now<<1|1])//相等取右侧
	{
		minn[now]=minn[now<<1]+lazy[now<<1];
		minp[now]=minp[now<<1];
	}
	else
	{
		minn[now]=minn[now<<1|1]+lazy[now<<1|1];
		minp[now]=minp[now<<1|1];
	}
}
void add_rgs(int now,int head,int tail,int left,int right,int v)
{
	if(head==left&&tail==right)
	{
		lazy[now]+=v;
		if(head==tail)
			merge_rgs(now,head);
		else
			merge_rgs(now,-1);
		return;
	}
	int mid=head+tail>>1;
	if(right<=mid)
	{
		add_rgs(now<<1,head,mid,left,right,v);
		merge_rgs(now,-1);
		return;
	}
	if(left>mid)
	{
		add_rgs(now<<1|1,mid+1,tail,left,right,v);
		merge_rgs(now,-1);
		return;
	}
	add_rgs(now<<1,head,mid,left,mid,v);
	add_rgs(now<<1|1,mid+1,tail,mid+1,right,v);
	merge_rgs(now,-1);
}
node ask_rgs(int now,int head,int tail,int left,int right)//相等取右侧 
{
	node ret,ret2;
	if(head==left&&tail==right)
	{
		ret.key=minn[now]+lazy[now];
		ret.pla=minp[now];
		return ret;
	}
	int mid=head+tail>>1;
	if(right<=mid)
	{
		ret=ask_rgs(now<<1,head,mid,left,right);
		ret.key+=lazy[now];
		return ret;
	}
	if(mid<left)
	{
		ret=ask_rgs(now<<1|1,mid+1,tail,left,right);
		ret.key+=lazy[now];
		return ret;
	}
	ret=ask_rgs(now<<1,head,mid,left,mid);
	ret2=ask_rgs(now<<1|1,mid+1,tail,mid+1,right);
	if(ret.key>=ret2.key)
		ret=ret2;
	ret.key+=lazy[now];
	return ret;
}
void change_max(int now,int head,int tail,int p,int v)//相等取右侧 
{
	if(head==tail)
	{
		maxx[now]=v;
		maxp[now]=p;
		return;
	}
	int mid=head+tail>>1;
	if(p<=mid)
		change_max(now<<1,head,mid,p,v);
	else
		change_max(now<<1|1,mid+1,tail,p,v);
	if(maxx[now<<1]>maxx[now<<1|1])
	{
		maxx[now]=maxx[now<<1];
		maxp[now]=maxp[now<<1];
	}
	else
	{
		maxx[now]=maxx[now<<1|1];
		maxp[now]=maxp[now<<1|1];
	}
}
node ask_max(int now,int head,int tail,int left,int right)
{
	node ret,ret2;
	if(head==left&&tail==right)
	{
		ret.key=maxx[now];
		ret.pla=maxp[now];
		return ret;
	}
	int mid=head+tail>>1;
	if(right<=mid)
		return ask_max(now<<1,head,mid,left,right);
	if(mid<left)
		return ask_max(now<<1|1,mid+1,right,left,right);
	ret=ask_max(now<<1,head,mid,left,mid);
	ret2=ask_max(now<<1|1,mid+1,tail,mid+1,right);
	if(ret.key>ret2.key)
		return ret;
	return ret2;
}
void solve(node res)
{
	int &i=res.pla;
	ANS+=res.key;
	if(sp[i])
		sp[i]=0;
	else
		--cnt[i];
	if(cnt[i]==0)
	{
		if(x[i]==0)
			return;
		--pla[i];
		cnt[i]=x[i];
	}
	if(pla[i]>0)
	{
		res.key=a[i];
		w[pla[i]].push(res);
		change_max(1,0,MAXP+1,pla[i],w[pla[i]].top().key);
	}
}
void cost_flow(int idx)
{
	int lim_p,max_p;
	node res;
	for(int i=1;i<=m;++i)
	{
		res=ask_rgs(1,0,MAXP+1,0,idx-1);//lim_p - 下标 
		if(res.key==0)
			lim_p=res.pla;
		else
			lim_p=idx-1;
		max_p=ask_max(1,0,MAXP+1,lim_p+1,MAXP+1).pla;//max_p - 下标 
		if(w[max_p].size()==0)
			return;
		res=w[max_p].top();
		w[max_p].pop();
		if(w[max_p].size()>0)
			change_max(1,0,MAXP+1,max_p,w[max_p].top().key);
		else
			change_max(1,0,MAXP+1,max_p,-INF);
		if(max_p<idx)
			add_rgs(1,0,MAXP+1,max_p,idx-1,-1);
		if(max_p>idx)
			add_rgs(1,0,MAXP+1,idx,max_p-1,1);
		solve(res);
	}
}
void init()
{
	int i,j;
	scanf("%d%d%d",&n,&m,&k);
	for(i=1;i<=n;++i)
		scanf("%d%d%d%d",a+i,s+i,f+i,x+i);
	for(i=1;i<=k;++i)
	{
		scanf("%d",q+i);
		if(q[i]>MAXP)
			MAXP=q[i];
	}
	for(i=1;i<=n;++i)
	{
		if(f[i]-MAXP*x[i]>0)
		{
			pla[i]=MAXP+1;
			cnt[i]=f[i]-MAXP*x[i]-1;
			sp[i]=1;
		}
		else
		{
			pla[i]=(f[i]+x[i]-1)/x[i];
			cnt[i]=f[i]-(pla[i]-1)*x[i]-1;
			sp[i]=1;
		}
	}
	node tmp;
	for(i=1;i<=n;++i)
	{
		tmp.key=a[i]+s[i];
		tmp.pla=i;
		w[pla[i]].push(tmp);
	}
	build_tree(1,0,MAXP+1);
}
//f[i]>0,x[i]>=0 
//每个菜都是从后向前加的，能吃后面的就能吃前面的，所以一定有东西吃
//每个位置维护一个堆 
int main()
{
	init();
	int i,j;
	for(i=1;i<=MAXP;++i)
	{
		cost_flow(i);
		ans[i]=ANS;
	}
	for(i=1;i<=k;++i)
		printf("%lld\n",ans[q[i]]);
	return 0;
}

