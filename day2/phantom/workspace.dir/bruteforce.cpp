#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#define CLOCKWISE 1
#define ANTICLOCKWISE -1
#define LEFT_CONVEX 1
#define RIGHT_CONVEX -1
#define CONSTRUCT_LAYERS 1
#define SINGLE_QUERY -1
#define MAX_N 100000
#define MAX_LAYERS 1
#define INF (~0U>>1)
using namespace std;

struct Point {
	int x, y;
	int layer, pos;
	int idx;
};
long long cx(Point *a, Point *b, Point *c) {
	return (long long) (b->x-a->x)*(c->y-a->y) - (long long) (b->y-a->y)*(c->x-a->x);
}
struct Hull {
	Point **p;
	int n;
	Hull (int n) {
		p = (Point **) malloc(sizeof(Point *) * (n+2));
		this->n = n;
	}
	Point *pos(int x) { while (x <= 0) x += n; while (x >= n) x -= n; return p[x]; }
};
struct Convex {
	Point **p;
	int h, t;
	int sign;
	bool refine(Point *a) { while (h < t && cx(p[t-1], p[t], a) * sign >= 0) --t; }
	void push(Point *a)   { refine(a); p[++t] = a; }
	Point *peek() 		  { return h == t ? NULL : p[h+1]; }
	Point *pop()          { return p[++h]; }
};
struct Opening {
	Point *a, *b;
	int apos, bpos;
	Convex *a_con, *b_con;
};
struct Segment {
	int l, r;
};

long long *v[MAX_LAYERS+10];
long long area;
int removed[MAX_N+10];
int idx[MAX_N+10];

Point *o;
Point *p[MAX_N+10], *p2[MAX_N+10], *q[MAX_N+10], *rp[MAX_N+10];
Point **point_heap;
int p_point_heap;

Hull *layer[MAX_LAYERS+10];

Opening *opening[MAX_LAYERS+10];
Opening **s_op, **t_op, **n_op;

Convex *convex[MAX_LAYERS+10];
int n_convex;

Segment segment[MAX_LAYERS+10];


bool cmp_pointrp(Point *a, Point *b) {
	return a->layer == b->layer ? a->pos < b->pos : a->layer < b->layer;
}
bool cmp_point2d(Point *a, Point *b) {
	return a->x == b->x ? a->y < b->y : a->x < b->x;
}
bool cmp_opening(Opening *a, Opening *b) {
	return a->apos < b->apos;
}
void init() {
	o = new Point();
	o->x = o->y = 0;
	point_heap = (Point **) malloc(sizeof(Point*) * (4 * MAX_LAYERS * MAX_LAYERS));
	opening[1] = new Opening();
	opening[1]->apos = -INF;
	opening[1]->bpos = INF;
}
void clear_for_query() {
	s_op = n_op = opening;
	t_op = s_op+1;
	p_point_heap = 0;
	area = 0;
}

void bruteforce(int n, Point **p, int &t, Point **q, int mode) {
	int h;
	h = 1, t = 0;
	for (int i = 1; i <= n; ++i) {
		if (mode == CONSTRUCT_LAYERS && p[i]->layer != 0)
			continue;
		if (mode == SINGLE_QUERY && removed[p[i]->idx] == true)
			continue;
		while (t > 1 && cx(q[t-1], q[t], p[i]) >= 0) --t;
		q[++t] = p[i];
	}
	h = t, --t;
	for (int i = n; i >= 1; --i) {
		if (mode == CONSTRUCT_LAYERS && p[i]->layer != 0)
			continue;
		if (mode == SINGLE_QUERY && removed[p[i]->idx] == true)
			continue;
		while (h < t && cx(q[t-1], q[t], p[i]) >= 0) --t;
		q[++t] = p[i];
	}
	if (t <= 0) t = 0;
	if (t == 1) t = 2, q[t] = q[1];
}
int main() {
	init();
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; ++i) {
		int x, y;
		Point *pt = new Point();
		scanf("%d %d", &x, &y);
		pt->x = x, pt->y = y;
		pt->idx = i;
		p[i] = p2[i] = pt;
	}
	sort(p2+1, p2+n+1, cmp_point2d);
	for (int i = 1; i <= m; ++i) {
		int rc = i == 1 ? -1 : (-area) % n;
		clear_for_query();
		
		int x, k, t = 0;
		scanf("%d", &k);
		for (int j = 1; j <= k; ++j) {
			scanf("%d", idx+j);
			idx[j] = (idx[j] + rc) % n + 1;
			if (p[idx[j]]->layer != 0)
				rp[++t] = p[idx[j]];
		}
		for (int j = 1; j <= k; ++j) removed[idx[j]] = true;
		bruteforce(n, p2, t, q, SINGLE_QUERY);
		for (int j = 1; j <= k; ++j) removed[idx[j]] = false;
		for (int i = 2; i <= t; ++i)
			area += cx(o, q[i-1], q[i]);
		cout << -area << endl;
	}
}
