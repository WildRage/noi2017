import os, sys
from tqdm import tqdm

if __name__ == '__main__':
	
	os.system('g++ -o bruteforce bruteforce.cpp -O2')
	os.system('g++ -o maker maker.cpp -O2')
	os.system('g++ -o phantom phantom.cpp -O2')
	os.system('g++ -o phantom-encode phantom-encode.cpp -O2')
	
	if os.path.exists(sys.argv[1]) == False:
		os.makedirs(sys.argv[1])
	
	s = [[10, 10, 0], 
		 [1000, 1000, 0],
		 [1000, 1000, 0],
		 [1000, 1000, 0],
		 [50000, 100000, 1],
		 [60000, 100000, 1],
		 [60000, 100000, 1],
		 [70000, 100000, 1],
		 [50000, 100000, 2],
		 [60000, 100000, 2],
		 [50000, 100000, 3],
		 [53000, 100000, 5],
		 [56000, 100000, 9],
		 [60000, 100000, 12],
		 [100000, 100000, 20],
		 [80000, 100000, 0],
		 [85000, 100000, 0],
		 [90000, 100000, 0],
		 [95000, 100000, 0],
		 [100000, 100000, 0]];
		
	seeds = [1705509164, 1851121541, 1001444947, 1245337339, 1765423261, 1327077039, 1784398296, 1275088877, 1124058630, 1598892733, 1380443466, 1161505343, 1980215358, 1644922834, 1286542235, 1288797991, 1470713522, 1967108678, 1721982884, 1251318927]
	
	for i in tqdm(range(20)):
		n, m, k = s[i]
		seed = seeds[i]
		output = 'phantom{}-decoded.in'.format(i+1)
		if k == 0:
			os.system('./maker {} {} -seed={} >{}'.format(n, m, seed, os.path.join(sys.argv[1], output)))
		else:
			os.system('./maker {} {} -k_max={} -seed={} >{}'.format(n, m, k, seed, os.path.join(sys.argv[1], output)))
		
		output2 = 'phantom{}.in'.format(i+1)	
		output3 = 'phantom{}.ans'.format(i+1)	
		os.system('./phantom-encode <{} >{}'.format(os.path.join(sys.argv[1], output), os.path.join(sys.argv[1], output2)))
		os.system('./phantom <{} >{}'.format(os.path.join(sys.argv[1], output2), os.path.join(sys.argv[1], output3)))
		os.system('rm -r {}'.format(os.path.join(sys.argv[1], output)))
		
	os.system('rm -r bruteforce maker phantom phantom-encode')

