#include <set>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
using namespace std;

int n, m, k, idx;
set < pair <long long, int> > st;
int cnt = 0;

int main() {
	cin >> n >> m;
	for (int i = 1; i <= n; ++i) {
		int x, y;
		scanf("%d %d", &x, &y);
	}
	for (int i = 1; i <= m; ++i) {
		scanf("%d", &k);
		long long h = 0;
		for (int j = 1; j <= k; ++j) {
			scanf("%d", &idx);
			h = (h * 131313 + idx) % ((int) 1e9 + 7);
		}
		pair <long long, int> p = make_pair(h, k);
		if (st.find(p) == st.end())
			++cnt, st.insert(p);
	}
	cerr << cnt << endl;
}
