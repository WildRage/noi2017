#include<iostream>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<algorithm>
#include<cassert>
using namespace std;
struct point{
	int x,y,where;
	void init(int k){
		where=k; scanf("%d%d",&x,&y);
		assert(-1e8<=x&&x<=1e8&&-1e8<=y&&y<=1e8);
	}
}x[210000],s[210000],A[210000];
int compare(point k1,point k2){
	return k1.x<k2.x;
}
int n,m,totK,pd[210000],sign,bo[210000],totnum;
point operator - (point k1,point k2){
	return (point){k1.x-k2.x,k1.y-k2.y,0};
}
long long cross(point k1,point k2){
	return 1ll*k1.x*k2.y-1ll*k2.x*k1.y;
}
long long calc(int flag){
	int head=0;
	for (int now=1;now<=n;now++)
		if (pd[x[now].where]!=sign){
			while ((head&&x[now].x==s[head].x&&x[now].y>=s[head].y)||
			(head>1&&cross(s[head]-s[head-1],x[now]-s[head])>=0)) head--;
			s[++head]=x[now];
		//	cout<<"fa"<<endl;
		//	for (int i=1;i<=head;i++) cout<<s[i].where<<" "; cout<<endl; 
		}
	int len=0;
	for (int i=1;i<=head;i++) A[++len]=s[i];
	head=0;
	for (int now=1;now<=n;now++)
		if (pd[x[now].where]!=sign){
			while ((head&&x[now].x==s[head].x&&x[now].y<=s[head].y)||
			(head>1&&cross(s[head]-s[head-1],x[now]-s[head])<=0)) head--;
			s[++head]=x[now];
		}
	for (int i=head;i;i--) A[++len]=s[i];
	long long s=0; A[len+1]=A[1];
	for (int i=1;i<=len;i++) s+=cross(A[i+1],A[i]);
//	for (int i=1;i<=len;i++) cout<<A[i].where<<" "; cout<<endl; 
	if (flag){
		for (int i=1;i<=len;i++)
			if (bo[A[i].where]==0){
				totnum++; bo[A[i].where]=1;
			}
	}
	return abs(s);
}
int get(char* s){
	int len=strlen(s),ans=0;
	for (int i=0;i<len;i++) ans=ans*10+s[i]-'0';
	return ans;
}
int main(int argc, char **argv) {
//	freopen("phantom2.in","r",stdin);
//	freopen("phantom2.out","w",stdout);
	int limn=200000,limm=200000,limK=200000;
//	int limn=get(argv[0]),limm=get(argv[1]),limK=get(argv[2]);
	scanf("%d%d",&n,&m);
	assert(1<=n&&n<=limn&&1<=m&&m<=limm);
	for (int i=1;i<=n;i++) x[i].init(i);
	sort(x+1,x+n+1,compare); sign++;
	assert(calc(1)>0); long long lastans=-1;
	for (;m;m--){
		int K; scanf("%d",&K); sign++; int rem=totnum;
		totK+=K; assert(K<=min(100,limK));
		for (;K;K--){
			long long k; scanf("%lld",&k); 
			assert(k>=0&&k<(1ll<<31));
			k=(k+lastans+n)%n+1;
			assert(k>=1&&k<=n);
			if (pd[k]!=sign){
				pd[k]=sign; if (bo[k]) rem--;
			}
		}
		assert(rem>=2);
		lastans=calc(0);
		printf("%lld\n",lastans);
	}
	assert(totK<=2000000);
	return 0;
}
